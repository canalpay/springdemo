package com.example;

/**
 * Created by CAN-Windows on 9.07.2016.
 */
import com.example.pjos.DAO.DenemeRepository;
import com.example.pjos.Deneme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestFull {


    @Autowired
    DenemeRepository denemeRepository;
    @RequestMapping("/Merhaba")
    public Deneme deneming(@RequestParam(value = "name", defaultValue = "ad") String ad){
        return new Deneme(ad);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Deneme add(
            @RequestParam(value = "name",defaultValue = "can") String ad
    ){
        Deneme deneme = new Deneme(ad);
        System.out.println(deneme);
        denemeRepository.save(deneme);
        return deneme;
    }
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<Deneme> getAll(){
        return denemeRepository.findAll();
    }



}
